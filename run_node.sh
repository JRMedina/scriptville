#!/usr/bin/env bash

# If no file passed, start docker in bash
if [ $# -eq 0 ]; then
    docker run -it --rm \
        --network=host \
        --name node-server \
        -v "$PWD":/usr/src/app \
        -w /usr/src/app \
        node:latest \
        bash
# If a node app.js file was passed, run that with node
else
    docker run -it --rm \
        --network=host \
        --name node-server \
        -v "$PWD":/usr/src/app \
        -w /usr/src/app \
        node:latest \
        node $1
fi