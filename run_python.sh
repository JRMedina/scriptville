#!/usr/bin/env bash

docker run -it --rm \
    --name python-docker \
    -v "$PWD":/usr/src/myapp \
    -w /usr/src/myapp \
    python:3 \
    python $1