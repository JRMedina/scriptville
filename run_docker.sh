#!/usr/bin/env bash

# Allow container to use host screen
xhost +

# Start container, mounting current directory, sharing necessary display files, sharing host network
docker run -it --rm \
    -v $(pwd):/$(basename "$(pwd)"):Z \
    -e DISPLAY=unix$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix:Z \
    --network=host \
    -w=/$(basename "$(pwd)") \
    $1 bash
