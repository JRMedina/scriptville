#!/usr/bin/env bash

# If no arg passed, start docker detatched
if [ $# -eq 0 ]; then
# Create container, and have it start running the mongo daemon
    docker run -it --rm \
        --network=host \
        -d mongo:latest \
        mongod;
    # Give time for mongo daemon to start
    sleep 1;
# if first arg is 'bash', place terminal in docker at bash terminal
elif [ $1 == "bash" ]; then
    # Create container, and have it start running the mongo daemon
    docker run -it --rm \
        --network=host \
        --name mongo-bash \
        -d mongo:latest \
        mongod;
    # Give time for mongo daemon to start
    sleep 1;
    # Place terminal at bash shell
    docker exec -it \
        mongo-bash \
        bash;
    # Stop mongo docker after
    docker stop mongo-bash; 
# if first arg is 'shell', place terminal in docker at mongo shell terminal
elif [ $1 == "shell" ]; then
    # Create container, and have it start running the mongo daemon
    docker run -it --rm \
        --network=host \
        --name mongo-shell \
        -d mongo:latest \
        mongod;
    # Give time for mongo daemon to start
    sleep 1;
    # Place terminal at mongo shell
    docker exec -it \
        mongo-shell \
        mongo;
    # Stop mongo docker after
    docker stop mongo-shell; 
else
    echo "Unknown combo of args for run_mongo.sh, try again..."
fi
